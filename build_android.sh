#!/bin/bash

export ANDROID_SDK_ROOT=/home/sergv/apps/Android/Sdk
export ANDROID_NDK_TOOLCHAIN_VERSION=4.9

WORKSPACE=/home/sergv/workspace
APP=$WORKSPACE/languik
KEYSTORE=$WORKSPACE/vlasovsoft/Chess/android/pbchess.keystore

for arch in armv7 x86 arm64_v8a x86_64; do
    for obj in animals fruits; do
        rm -rf obj_${arch}_${obj}
        mkdir -p obj_${arch}_${obj}
        pushd obj_${arch}_${obj}
        case $arch in
            armv7)
                qtver=5.12.6
                ;;
            x86)
                qtver=5.12.6
                ;;
            arm64_v8a)
                qtver=5.12.6
                ;;
            x86_64)
                qtver=5.13.2
                ;;
        esac
        QT=/home/sergv/apps/Qt/$qtver
        $QT/android_$arch/bin/qmake OBJECT=$obj $APP/languik.pro
        make
        mkdir -p android-build
        make INSTALL_ROOT=`pwd`/android-build install
        mkdir -p android-build/assets
        cp *.qm android-build/assets
        cp $APP/resources/${obj}/*.{jpg,mp3} android-build/assets
        $QT/android_$arch/bin/androiddeployqt --input `pwd`/android-liblanguik.so-deployment-settings.json --output `pwd`/android-build --deployment bundled --android-platform android-24 --jdk /usr --gradle --sign $KEYSTORE pbchess --storepass ufopedia
        cp android-build/build/outputs/apk/release/android-build-release-signed.apk ../${obj}_${arch}.apk
        popd
    done
done

xclock
