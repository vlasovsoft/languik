#include <QDebug>
#include <QFile>
#include <QSettings>
#include <QCoreApplication>

#include "utils.h"

#include "myobject.h"

extern QSettings* g_pConfig;

MyObject::MyObject(QObject *parent)
    : QObject(parent)
    , questionNo(0)
    , hints(0)
    , errors(0)
    , done(false)
{
    // read objects
     QFile file(":/list.txt");
     file.open(QIODevice::ReadOnly);
     if ( file.isOpen() )
     {
         QTextStream in(&file);
         while(!in.atEnd()) {
             QString line = in.readLine().trimmed();
             if ( !line.isEmpty() )
                 objects.append(line);
         }
     }
}

QString MyObject::animal(int x, int y) const
{
    return objects[level[questionNo].board[y][x]];
}

QString MyObject::animal() const
{
    return objects[level[questionNo].question];
}

bool MyObject::click(int x, int y) const
{
    return level[questionNo].board[y][x] == level[questionNo].question;
}

void MyObject::initLevel()
{
    srand(time(NULL));

    level.clear();
    questionNo = 0;
    hints = errors = 0;
    done = false;

    std::vector<int> vec;
    int count = 3 + getLevel();

    for ( int i=0; i<count; ++i )
    {
        vec.push_back( i );
    }

    std::random_shuffle(vec.begin(), vec.end());

    for ( int i=0; i<count; ++i )
    {
        Task task;
        task.question = vec[i];
        std::vector<int> v(vec);
        std::random_shuffle(v.begin(), v.end() );
        if ( v[0]!=task.question &&
             v[1]!=task.question &&
             v[2]!=task.question &&
             v[3]!=task.question )
        {
            v[rand() % 4] = task.question;
        }
        task.board[0][0] = v[0];
        task.board[0][1] = v[1];
        task.board[1][0] = v[2];
        task.board[1][1] = v[3];
        level.push_back(task);
    }
}

bool MyObject::nextQuestion()
{
    return ( ++questionNo < level.size() );
}

int MyObject::getLevelsCount() const
{
    return objectsCount() - 3;
}

int MyObject::objectsCount() const
{
    return objects.size();
}

int MyObject::getLevel() const
{
    int level = g_pConfig->value("level", 1).toInt();
    if ( level < 1 || level > getLevelsCount() )
        level = 1;
    return level;
}

void MyObject::setLevel(int lvl)
{
    g_pConfig->setValue("level", lvl);
    level.clear();
    questionNo = 0;
    hints = errors = 0;
    done = false;
}

void MyObject::incLevel()
{
    int level = getLevel();
    if ( level < getLevelsCount() )
    {
        g_pConfig->setValue("level", level+1);
    }
}

int MyObject::getHints() const
{
    return hints;
}

void MyObject::incHints()
{
    hints++;
}

int MyObject::getErrors() const
{
    return errors;
}

void MyObject::incErrors()
{
    errors++;
}

bool MyObject::speakText()
{
    return g_pConfig->value("speak_text", true).toBool();
}

void MyObject::setSpeakText(bool val)
{
    g_pConfig->setValue("speak_text", val);
}

bool MyObject::showText()
{
    return g_pConfig->value("show_text", true).toBool();
}

void MyObject::setShowText(bool val)
{
    g_pConfig->setValue("show_text", val);
}

QString MyObject::root() const
{
    return get_qml_root();
}

