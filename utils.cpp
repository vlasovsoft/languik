#include <QLocale>
#include <QCoreApplication>

#include "utils.h"

QString get_root()
{
#if defined(ANDROID)
    return "assets:";
#else
    return QCoreApplication::applicationDirPath();
#endif
}

QString get_qml_root()
{
#if defined(ANDROID)
    return "assets:";
#else
    return QString("file://") + QCoreApplication::applicationDirPath();
#endif
}

QString get_lang()
{
    return QLocale::languageToString(QLocale::system().language());
}
