#ifndef APPLICATION_H
#define APPLICATION_H

#include <QTranslator>
#include <QGuiApplication>

class Application : public QGuiApplication
{
    Q_OBJECT

    QTranslator trans;

public:
    Application(int &argc, char **argv);
    ~Application();

private slots:
    void stateChanged(Qt::ApplicationState state);
};

#endif // APPLICATION_H
