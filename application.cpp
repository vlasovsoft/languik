#include <QSettings>

#include "application.h"

QSettings* g_pConfig;

#define STRINGIFY(x) #x
#define STR(x) STRINGIFY(x)

Application::Application(int &argc, char **argv)
    : QGuiApplication(argc,argv)
{
    setOrganizationName("vlasovsoft");
    setApplicationName(STR(OBJECT));
    g_pConfig = new QSettings(organizationName(), applicationName());

    if (    !trans.load(":/trans.qm")
         || !installTranslator(&trans) )
    {
        qCritical("Failed to load translations!");
    }

    QObject::connect( this, SIGNAL(applicationStateChanged(Qt::ApplicationState)), this, SLOT(stateChanged(Qt::ApplicationState)) );
}

Application::~Application()
{
    delete g_pConfig;
    g_pConfig = NULL;
}

void Application::stateChanged(Qt::ApplicationState state)
{
    if ( Qt::ApplicationSuspended == state )
    {
        g_pConfig->sync();
    }
}
