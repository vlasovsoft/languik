#!/bin/sh
set -e
rm -f *.ipa

export PATH=$PATH:/Users/sergv/Qt/5.6/ios/bin

rm -rf out_app
mkdir -p out_app

pushd ../resources/i18n
lrelease *.ts
popd

security -v unlock-keychain -p "ufopedia" "$HOME/Library/Keychains/login.keychain"

pushd out_app
qmake OBJECT=animals CONFIG+="iphoneos release" ../..
popd
