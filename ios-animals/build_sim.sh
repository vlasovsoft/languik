#!/bin/sh
set -e
rm -f *.ipa

export PATH=$PATH:/Users/sergv/Qt/5.6/ios/bin

rm -rf out_sim
mkdir -p out_sim

pushd ../resources/i18n
lrelease *.ts
popd

pushd out_sim
qmake OBJECT=animals CONFIG+=iphonesimulator ../..
make
popd

make OBJECT=animals SIM=1
