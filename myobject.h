#ifndef MYOBJECT_H
#define MYOBJECT_H

#include <QObject>
#include <QVector>

struct Task
{
    int question;
    int board[2][2];
};

class MyObject : public QObject
{
    Q_OBJECT

    int questionNo;
    QVector<Task> level;
    QVector<QString> objects;
    int hints;
    int errors;
    bool done;

public:
    explicit MyObject(QObject *parent = 0);
    Q_INVOKABLE QString animal(int x, int y) const;
    Q_INVOKABLE QString animal() const;
    Q_INVOKABLE bool click(int x, int y) const;
    Q_INVOKABLE void initLevel();
    Q_INVOKABLE bool nextQuestion();
    Q_INVOKABLE int getLevelsCount() const;
    Q_INVOKABLE int objectsCount() const;
    Q_INVOKABLE int getLevel() const;
    Q_INVOKABLE void setLevel( int lvl );
    Q_INVOKABLE void incLevel();
    Q_INVOKABLE int getHints() const;
    Q_INVOKABLE void incHints();
    Q_INVOKABLE int getErrors() const;
    Q_INVOKABLE void incErrors();
    Q_INVOKABLE void setLevelDone() { done = true; }
    Q_INVOKABLE bool levelDone() { return done; }
    Q_INVOKABLE bool speakText();
    Q_INVOKABLE void setSpeakText(bool val);
    Q_INVOKABLE bool showText();
    Q_INVOKABLE void setShowText(bool val);
    Q_INVOKABLE QString root() const;
};

#endif // MYOBJECT_H
