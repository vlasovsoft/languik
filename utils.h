#ifndef UTILS_H
#define UTILS_H

#include <QString>

QString get_root();
QString get_qml_root();
QString get_lang();

#endif // UTILS_H
