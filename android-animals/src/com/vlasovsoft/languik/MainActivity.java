package com.vlasovsoft.languik;

import android.os.Bundle;

public class MainActivity extends org.qtproject.qt5.android.bindings.QtActivity {
    
    // ======================== Activity interface =======================
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
 
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

