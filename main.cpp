#include <QQmlApplicationEngine>
#include <QtQml>
#include <QDir>

#include "utils.h"
#include "application.h"
#include "myobject.h"

int main(int argc, char *argv[])
{
    Application app(argc, argv);

    QTranslator translator;
    if (translator.load( get_root() + QDir::separator() + get_lang() + ".qm" ))
        app.installTranslator(&translator);

    qmlRegisterType<MyObject>("com.vlasovsoft.myobject", 1, 0, "MyObject");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
