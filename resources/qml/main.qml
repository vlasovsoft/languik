import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import com.vlasovsoft.myobject 1.0

ApplicationWindow {
    visible: true

    width: 800;
    height: 600;

    id: main;

    MyObject {
        id: obj;
    }

    StackView {
        anchors.fill: parent;
        id: loader;

        initialItem: Results {
            id: results;
        }

        Settings {
            id: settings
            objectName: "settings"
        }
    }

    onClosing: {
        if (loader.depth > 1) {
            close.accepted = false
            loader.pop();
            results.update();
        }
        else {
          return;
        }
    }
}
