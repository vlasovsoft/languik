import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.2
import QtMultimedia 5.5

Item {

    id: level;

    property bool soundFinished;
    property bool textFinished;

    Component.onCompleted: {
        obj.initLevel();
        initQuestion();
    }

    Audio {
        id: sound;
        onStopped: {
            soundFinished = true;
        }
    }

    SoundEffect {
        id: error;
        source: "qrc:/incorrect.wav";
    }

    Image {
        id: heart;

        property int destWidth;
        property int destHeight;

        source: "qrc:/heart.png";
        fillMode: Image.PreserveAspectFit;
        rotation: 180;
        visible: false;
        opacity: 0.0;
        states: State {
            name: "visible"
            PropertyChanges { target: heart; rotation: 0 }
            PropertyChanges { target: heart; width: destWidth; height: destHeight }
            PropertyChanges { target: heart; visible: true }
            PropertyChanges { target: heart; opacity: 1.0 }
        }
        transitions: Transition {
            ParallelAnimation {
                RotationAnimation { duration: 500; direction: RotationAnimation.Counterclockwise }
                PropertyAnimation { duration: 500; properties: "width, height, opacity" }
            }
        }
    }

    Timer {
        id: soundTimer;
        repeat: false;
        interval: 1000;
        onTriggered: {
            sound.play();
        }
    }

    Timer {
        id: questionTimer;
        repeat: false;
        interval: 600;
        onTriggered: {
            if ( obj.nextQuestion() )
            {
                initQuestion();
            }
            else
            {
                obj.setLevelDone();
                results.update();
                results.playSound();
                loader.pop();
            }
        }
    }

    Timer {
        id: textTimer;
        repeat: false;
        interval: 3000;
        onTriggered: {
            hideText();
        }
    }

    function speakText() {
        sound.source = "";
        sound.source = obj.root() + "/" + obj.animal() + ".mp3";
        soundTimer.start();
    }

    function showHint() {
        if ( obj.animal() == obj.animal(0,0) )
        {
            rImg0.color = "red";
        }
        else
        if ( obj.animal() == obj.animal(0,1) )
        {
            rImg1.color = "red";
        }
        else
        if ( obj.animal() == obj.animal(1,0) )
        {
            rImg2.color = "red";
        }
        else
        if ( obj.animal() == obj.animal(1,1) )
        {
            rImg3.color = "red";
        }
        obj.incHints();
    }

    function hideHint() {
        rImg0.color = "transparent";
        rImg1.color = "transparent";
        rImg2.color = "transparent";
        rImg3.color = "transparent";
    }

    function showHeart(img) {
        heart.parent = img;
        heart.anchors.centerIn = img;
        heart.width = img.width / 4;
        heart.height = img.height / 4;
        heart.destWidth = img.width / 2;
        heart.destHeight = img.height / 2;
        heart.state = "visible";
    }

    function hideHeart(img) {
        heart.state = "default";
        heart.anchors.centerIn = undefined;
        heart.parent = level;
        heart.visible = false;
    }

    function showText() {
        label.text = obj.animal();
        text.state = "visible";
        textTimer.start();
    }

    function hideText() {
        textTimer.stop();
        text.state = "";
    }

    function initQuestion() {
        soundFinished = false;
        hideHeart();
        hideText();
        hideHint();
        img0.source  = obj.root() + "/" + obj.animal(0,0) + ".jpg";
        img1.source  = obj.root() + "/" + obj.animal(0,1) + ".jpg";
        img2.source  = obj.root() + "/" + obj.animal(1,0) + ".jpg";
        img3.source  = obj.root() + "/" + obj.animal(1,1) + ".jpg";
        question();
        console.log("initQuestion()");
    }

    function question() {
        if ( obj.speakText() ) {
            speakText();
        }
        else {
            soundFinished = true;
        }
        if ( obj.showText() ) {
            showText();
        }
    }

    function clicked(img,x,y) {
        if (    questionTimer.running
             || error.playing
             || !soundFinished )
            return;

        if ( Audio.PlayingState == sound.playbackState )
            return;

        if ( obj.click(x,y) )
        {
            showHeart(img);
            questionTimer.start();
        }
        else
        {
            obj.incErrors();
            error.play();
        }
    }

    StateGroup {
         states: [
             State {
                 when: width >= height*640/480;
                 PropertyChanges { target: buttons1; visible: true }
                 PropertyChanges { target: buttons2; visible: false }
                 AnchorChanges { target: images; anchors.right: buttons1.left; anchors.bottom: parent.bottom; }
             },
             State {
                 when: width < height*640/480
                 PropertyChanges { target: buttons1; visible: false }
                 PropertyChanges { target: buttons2; visible: true }
                 AnchorChanges { target: images; anchors.bottom: buttons2.top; anchors.right: parent.right; }
             }
        ]
    }

    Rectangle
    {
        id: r0;
        anchors.fill: parent;

        Rectangle {
            id: images;
            color: "lightblue";

            anchors.left: parent.left;
            anchors.top: parent.top;
            anchors.right: buttons1.left;
            anchors.bottom: buttons2.top;

            Rectangle {
                id: text;
                z: 1.0;
                color: "yellow";
                anchors.centerIn: parent;
                width: label.width+20
                height: label.height+20
                border.width: 2;
                border.color: "black";
                opacity: 0.5;
                scale: 0.5;
                visible: false;
                Text {
                    id: label;
                    text: "Hello!";
                    anchors.centerIn: parent;
                    font.pointSize: 30;
                }
                states: State {
                    name: "visible"
                    PropertyChanges { target: text; visible: true }
                    PropertyChanges { target: text; opacity: 1.0 }
                    PropertyChanges { target: text; scale: 1.0 }
                }
                transitions: Transition {
                    from: "";
                    to: "visible";
                    PropertyAnimation { duration: 500; properties: "scale, opacity"; }
                }
            }

            GridLayout {
                columns: 2
                id: grid;
                anchors.centerIn: parent;
                rowSpacing: 0;
                columnSpacing: 0;

                height: ( parent.width > parent.height*640/480? parent.height         : parent.width*480/640 ) - 8;
                width:  ( parent.width > parent.height*640/480? parent.height*640/480 : parent.width ) - 8;

                Rectangle {
                    id: rImg0;
                    color: "transparent";
                    Layout.fillWidth: true;
                    Layout.fillHeight: true;
                    Image {
                        id: img0;
                        anchors.fill: parent;
                        anchors.margins: 4;
                        MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                level.clicked(img0,0,0);
                            }
                        }
                    }
                }
                Rectangle {
                    id: rImg1;
                    color: "transparent";
                    Layout.fillWidth: true;
                    Layout.fillHeight: true;
                    Image {
                        id: img1;
                        anchors.fill: parent;
                        anchors.margins: 4;
                        MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                level.clicked(img1,0,1);
                            }
                        }
                    }
                }
                Rectangle {
                    id: rImg2;
                    color: "transparent";
                    Layout.fillWidth: true;
                    Layout.fillHeight: true;
                    Image {
                        id: img2;
                        anchors.fill: parent;
                        anchors.margins: 4;
                        MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                level.clicked(img2,1,0);
                            }
                        }
                    }
                }
                Rectangle {
                    id: rImg3;
                    color: "transparent";
                    Layout.fillWidth: true;
                    Layout.fillHeight: true;
                    Image {
                        id: img3;
                        anchors.fill: parent;
                        anchors.margins: 4;
                        MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                level.clicked(img3,1,1);
                            }
                        }
                    }
                }
            }
        }

        Rectangle {
            id: buttons1;
            width: 60;
            color: "lightblue";

            anchors.top: parent.top;
            anchors.bottom: parent.bottom;
            anchors.right: parent.right;

            ColumnLayout {
                anchors.fill: parent;

                ToolButton {
                    onClicked: {
                        soundFinished = false;
                        question();
                    }
                    Layout.minimumWidth: 60
                    Layout.minimumHeight: 60
                    Layout.maximumWidth: 60
                    Layout.maximumHeight: 60
                    Image {
                        source: "qrc:/sound.png"
                        anchors.fill: parent
                        anchors.margins: 4
                    }
                }
                ToolButton {
                    onClicked: {
                        level.showHint();
                    }
                    Layout.minimumWidth: 60
                    Layout.minimumHeight: 60
                    Layout.maximumWidth: 60
                    Layout.maximumHeight: 60
                    Image {
                        source: "qrc:/hint.png"
                        anchors.fill: parent
                        anchors.margins: 4
                    }
                }
            }
        }

        Rectangle {
            id: buttons2;
            height: 60;
            color: "lightblue";

            anchors.bottom: parent.bottom;
            anchors.left: parent.left;
            anchors.right: parent.right;

            RowLayout
            {
                anchors.fill: parent;
                ToolButton {
                    onClicked: {
                        soundFinished = false;
                        question();
                    }
                    Layout.minimumWidth: 60
                    Layout.minimumHeight: 60
                    Layout.maximumWidth: 60
                    Layout.maximumHeight: 60
                    Layout.alignment: Qt.AlignHCenter
                    Image {
                        source: "qrc:/sound.png"
                        anchors.fill: parent
                        anchors.margins: 4
                    }
                }
                ToolButton {
                    onClicked: {
                        loader.source = "qrc:/qml/settings.qml";
                    }
                    Layout.minimumWidth: 60
                    Layout.minimumHeight: 60
                    Layout.maximumWidth: 60
                    Layout.maximumHeight: 60
                    Layout.alignment: Qt.AlignHCenter
                    Image {
                        source: "qrc:/settings.png"
                        anchors.fill: parent
                        anchors.margins: 4
                    }
                }
                ToolButton {
                    onClicked: {
                        level.showHint();
                    }
                    Layout.minimumWidth: 60
                    Layout.minimumHeight: 60
                    Layout.maximumWidth: 60
                    Layout.maximumHeight: 60
                    Layout.alignment: Qt.AlignHCenter
                    Image {
                        source: "qrc:/hint.png"
                        anchors.fill: parent
                        anchors.margins: 4
                    }
                }
            }
        }
    }
}
