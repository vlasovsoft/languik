import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.3

Item {
    function playSound() {
    }
    function update() {
        header.text = qsTr("Level %1").arg(obj.getLevel());
        comments.color = obj.getErrors() > 0 ?
            "red" :
            ( obj.getHints() > 0 ? "blue" : "green" );
        comments.visible = obj.levelDone();
        comments.text = obj.getErrors() > 0 ?
            qsTr("Level is failed! Errors: %1").arg(obj.getErrors()) :
            ( obj.getHints() > 0 ?
                  qsTr("Level is failed! Hints: %1").arg(obj.getHints()) :
                  qsTr("Level is done!") );
        btnRestart.visible = obj.levelDone();
        btnForward.visible =
                   0 == obj.getErrors() && 0 == obj.getHints()
                && ( obj.getLevel() < obj.getLevelsCount() || !obj.levelDone() );
        progressBar.value = obj.getLevel();
    }

    Component.onCompleted: {
        update();
    }

    Rectangle {
        anchors.fill: parent;
        color: "lightblue";
        ColumnLayout {
            anchors.fill: parent;
            Text {
                id: header;
                font.pointSize: 20;
                font.bold: true;
                Layout.alignment: Qt.AlignCenter;
                Layout.topMargin: 10;
                Layout.bottomMargin: 10;
            }
            ProgressBar {
                id: progressBar
                minimumValue: 0;
                maximumValue: obj.getLevelsCount();
                orientation: Qt.Horizontal;
                Layout.fillWidth: true;
                Layout.leftMargin: 10;
                Layout.rightMargin: 10;
            }
            Text {
                id: comments;
                font.pointSize: 15;
                Layout.alignment: Qt.AlignCenter;
            }
            Item {
                Layout.fillHeight: true;
                Layout.fillWidth: true;
                RowLayout {
                    anchors.centerIn: parent;
                    ToolButton {
                        id: btnSettings
                        onClicked: {
                            loader.push(settings);
                        }
                        Layout.minimumWidth: 96
                        Layout.minimumHeight: 96
                        Layout.alignment: Qt.AlignVCenter
                        Image {
                            source: "qrc:/settings.png"
                            anchors.fill: parent
                            anchors.margins: 4
                        }
                    }
                    ToolButton {
                        id: btnRestart
                        onClicked: {
                            obj.setLevel( obj.getLevel() );
                            loader.push("qrc:/qml/Level.qml");
                        }
                        Layout.minimumWidth: 96
                        Layout.minimumHeight: 96
                        Layout.alignment: Qt.AlignVCenter
                        Image {
                            source: "qrc:/restart.png"
                            anchors.fill: parent
                            anchors.margins: 4
                        }
                    }
                    ToolButton {
                        id: btnForward
                        onClicked: {
                            obj.setLevel( ( obj.levelDone()
                                          && obj.getLevel() < obj.getLevelsCount() ) ?
                                                 obj.getLevel()+1 :
                                                 obj.getLevel() );
                            loader.push("qrc:/qml/Level.qml");
                        }
                        Layout.minimumWidth: 96
                        Layout.minimumHeight: 96
                        Layout.alignment: Qt.AlignVCenter
                        Image {
                            source: "qrc:/forward.png"
                            anchors.fill: parent
                            anchors.margins: 4
                        }
                    }
                }
            }
        }
    }
}
