import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.2

Item {
    id: settings;

    function apply() {
        obj.setLevel(level.value);
        obj.setSpeakText(chkSpeakText.checked);
        obj.setShowText(chkShowText.checked);
        results.update();
    }

    ColumnLayout {
        anchors.fill: parent;
        anchors.margins: 10
        Label {
            text: qsTr("Level: ") + level.value;
        }
        Slider {
            id: level;
            minimumValue: 1;
            maximumValue: obj.getLevelsCount();
            stepSize: 1;
            value: obj.getLevel();
            Layout.fillWidth: true;
        }
        CheckBox {
            id: chkSpeakText;
            text: qsTr("Speak text");
            checked: obj.speakText();
            onClicked: {
                if ( !checked && !chkShowText.checked ) {
                    chkShowText.checked = true;
                }
            }
        }
        CheckBox {
            id: chkShowText;
            text: qsTr("Show text");
            checked: obj.showText();
            onClicked: {
                if ( !checked && !chkSpeakText.checked ) {
                    chkSpeakText.checked = true;
                }
            }
        }
        Item {
            Layout.fillHeight: true;
        }
        Button {
            id: play;
            text: qsTr("Play");
            onClicked: {
                apply();
                loader.pop();
            }
        }
    }
}
