#!/usr/bin/perl

$theme = shift;

@themes = qw(animals fruits);

foreach $theme (@themes) 
{
    open(IFILE,"<$theme/list.txt");
    open(OFILE, '>', "$theme".'.qrc') or die "Could not open file!";

    print OFILE "<RCC>\n";
    print OFILE "<qresource>\n";

    while (<IFILE>)
    {
        my $line = $_;
        chomp $line;
        if ($line ne "") {
            print OFILE '<file ' . 'alias="' . $line . '.jpg">' . $theme . '/' . $line . '.jpg' . "</file>\n";
            print OFILE '<file ' . 'alias="' . $line . '.mp3">' . $theme . '/' . $line . '.mp3' . "</file>\n";
        }
    }

    print OFILE '<file alias="list.txt">' . $theme . "/list.txt</file>\n";
    print OFILE "</qresource>\n";
    print OFILE "</RCC>";

    close OFILE;
    close IFILE;
}


