TEMPLATE = app

QT += qml quick

SOURCES += main.cpp myobject.cpp \
    application.cpp \
    utils.cpp

lupdate_only{
SOURCES += resources/qml/*.qml
}

ios {
    QMAKE_INFO_PLIST = ios-$${OBJECT}/Info.plist
    jpgFiles.path = .
    jpgFiles.files = $$files(resources/$${OBJECT}/*.jpg)
    mp3Files.path = .
    mp3Files.files = $$files(resources/$${OBJECT}/*.mp3)
    QMAKE_BUNDLE_DATA += jpgFiles mp3Files
}

HEADERS += myobject.h application.h \
    utils.h
DEFINES += OBJECT=$${OBJECT}
RESOURCES += resources/languik.qrc resources/$${OBJECT}.qrc 
TRANSLATIONS += i18n/English.ts \
                i18n/Russian.ts \
                i18n/Spanish.ts \
                i18n/French.ts  \
                i18n/German.ts

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android-$${OBJECT}

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

QMAKE_EXTRA_COMPILERS += lrelease
lrelease.input         = TRANSLATIONS
lrelease.output        = ${QMAKE_FILE_BASE}.qm
lrelease.commands      = $$[QT_INSTALL_BINS]/lrelease ${QMAKE_FILE_IN} -qm ${QMAKE_FILE_BASE}.qm
lrelease.CONFIG       += no_link target_predeps
