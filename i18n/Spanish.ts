<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>Results</name>
    <message>
        <location filename="../resources/qml/Results.qml" line="9"/>
        <source>Level %1</source>
        <translation>Nivel %1</translation>
    </message>
    <message>
        <location filename="../resources/qml/Results.qml" line="15"/>
        <source>Level is failed! Errors: %1</source>
        <translation>El nivel de ha sido fallado! Errores: %1</translation>
    </message>
    <message>
        <location filename="../resources/qml/Results.qml" line="17"/>
        <source>Level is failed! Hints: %1</source>
        <translation>El nivel de ha sido fallado! Ayuda: %1</translation>
    </message>
    <message>
        <location filename="../resources/qml/Results.qml" line="18"/>
        <source>Level is done!</source>
        <translation>El nivel se hace!</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../resources/qml/Settings.qml" line="19"/>
        <source>Level: </source>
        <translation>Nivel:</translation>
    </message>
    <message>
        <location filename="../resources/qml/Settings.qml" line="31"/>
        <source>Speak text</source>
        <translation>Pronunciar el texto</translation>
    </message>
    <message>
        <location filename="../resources/qml/Settings.qml" line="41"/>
        <source>Show text</source>
        <translation>Mostrar el texto</translation>
    </message>
    <message>
        <location filename="../resources/qml/Settings.qml" line="54"/>
        <source>Play</source>
        <translation>Jugar</translation>
    </message>
</context>
<context>
    <name>results</name>
    <message>
        <source>Level %1 is not completed!</source>
        <translation type="vanished">El nivel de %1 ha sido fallado!</translation>
    </message>
    <message>
        <source>Level %1 is completed!</source>
        <translation type="vanished">El nivel de %1 ha sido superado!</translation>
    </message>
    <message>
        <source>Errors: %1</source>
        <translation type="vanished">Errores: %1</translation>
    </message>
    <message>
        <source>Hints: %1</source>
        <translation type="vanished">Ayuda: %1</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <source>Level: </source>
        <translation type="vanished">Nivel:</translation>
    </message>
    <message>
        <source>Speak text</source>
        <translation type="vanished">Pronunciar el texto</translation>
    </message>
    <message>
        <source>Show text</source>
        <translation type="vanished">Mostrar el texto</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="vanished">Jugar</translation>
    </message>
</context>
</TS>
