<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>Results</name>
    <message>
        <location filename="../resources/qml/Results.qml" line="9"/>
        <source>Level %1</source>
        <translation>Level %1</translation>
    </message>
    <message>
        <location filename="../resources/qml/Results.qml" line="15"/>
        <source>Level is failed! Errors: %1</source>
        <translation>Level ist nicht abgeschlossen! Fehler: %1</translation>
    </message>
    <message>
        <location filename="../resources/qml/Results.qml" line="17"/>
        <source>Level is failed! Hints: %1</source>
        <translation>Level ist nicht abgeschlossen! Joker: %1</translation>
    </message>
    <message>
        <location filename="../resources/qml/Results.qml" line="18"/>
        <source>Level is done!</source>
        <translatorcomment>google translate</translatorcomment>
        <translation>Level ist fertig!</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../resources/qml/Settings.qml" line="19"/>
        <source>Level: </source>
        <translation>Level:</translation>
    </message>
    <message>
        <location filename="../resources/qml/Settings.qml" line="31"/>
        <source>Speak text</source>
        <translation>Text anhören</translation>
    </message>
    <message>
        <location filename="../resources/qml/Settings.qml" line="41"/>
        <source>Show text</source>
        <translation>Text anzeigen</translation>
    </message>
    <message>
        <location filename="../resources/qml/Settings.qml" line="54"/>
        <source>Play</source>
        <translation>Spielen</translation>
    </message>
</context>
<context>
    <name>results</name>
    <message>
        <source>Level %1 is not completed!</source>
        <translation type="vanished">Level %1 ist nicht abgeschlossen!</translation>
    </message>
    <message>
        <source>Level %1 is completed!</source>
        <translation type="vanished">Level %1 ist abgeschlossen!</translation>
    </message>
    <message>
        <source>Errors: %1</source>
        <translation type="vanished">Fehler: %1</translation>
    </message>
    <message>
        <source>Hints: %1</source>
        <translation type="vanished">Joker: %1</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <source>Level: </source>
        <translation type="vanished">Level:</translation>
    </message>
    <message>
        <source>Speak text</source>
        <translation type="vanished">Text anhören</translation>
    </message>
    <message>
        <source>Show text</source>
        <translation type="vanished">Text anzeigen</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="vanished">Spielen</translation>
    </message>
</context>
</TS>
