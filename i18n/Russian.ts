<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>Results</name>
    <message>
        <source>Level %1</source>
        <translation>Уровень %1</translation>
    </message>
    <message>
        <source>Level is failed! Errors: %1</source>
        <translation>Уровень не пройден! Ошибок: %1</translation>
    </message>
    <message>
        <source>Level is failed! Hints: %1</source>
        <translation>Уровень не пройден! Подсказок: %1</translation>
    </message>
    <message>
        <source>Level is done!</source>
        <translation>Уровень пройден!</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Level: </source>
        <translation>Уровень:</translation>
    </message>
    <message>
        <source>Speak text</source>
        <translation>Говорить текст</translation>
    </message>
    <message>
        <source>Show text</source>
        <translation>Показывать текст</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>Играть</translation>
    </message>
</context>
<context>
    <name>results</name>
    <message>
        <source>Level %1 is not completed!</source>
        <translation type="vanished">Уровень %1 не пройден!</translation>
    </message>
    <message>
        <source>Level %1 is completed!</source>
        <translation type="vanished">Уровень %1 пройден!</translation>
    </message>
    <message>
        <source>Errors: %1</source>
        <translation type="vanished">Ошибок: %1</translation>
    </message>
    <message>
        <source>Hints: %1</source>
        <translation type="vanished">Подсказок: %1</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <source>Level: </source>
        <translation type="vanished">Уровень:</translation>
    </message>
    <message>
        <source>Speak text</source>
        <translation type="vanished">Говорить текст</translation>
    </message>
    <message>
        <source>Show text</source>
        <translation type="vanished">Показывать текст</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="vanished">Играть</translation>
    </message>
</context>
</TS>
