<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>Results</name>
    <message>
        <location filename="../resources/qml/Results.qml" line="9"/>
        <source>Level %1</source>
        <translation>Niveau %1</translation>
    </message>
    <message>
        <location filename="../resources/qml/Results.qml" line="15"/>
        <source>Level is failed! Errors: %1</source>
        <translation>Le niveau n&apos;est pas passé! Erreurs: %1</translation>
    </message>
    <message>
        <location filename="../resources/qml/Results.qml" line="17"/>
        <source>Level is failed! Hints: %1</source>
        <translation>Le niveau n&apos;est pas passé! Invites: %1</translation>
    </message>
    <message>
        <location filename="../resources/qml/Results.qml" line="18"/>
        <source>Level is done!</source>
        <translation>Le niveau est terminé!</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../resources/qml/Settings.qml" line="19"/>
        <source>Level: </source>
        <translation>Niveau:</translation>
    </message>
    <message>
        <location filename="../resources/qml/Settings.qml" line="31"/>
        <source>Speak text</source>
        <translation>Dire le texte</translation>
    </message>
    <message>
        <location filename="../resources/qml/Settings.qml" line="41"/>
        <source>Show text</source>
        <translation>Montrer le texte</translation>
    </message>
    <message>
        <location filename="../resources/qml/Settings.qml" line="54"/>
        <source>Play</source>
        <translation>Jouer</translation>
    </message>
</context>
<context>
    <name>results</name>
    <message>
        <source>Level %1 is not completed!</source>
        <translation type="vanished">Le niveau %1 n&apos;est pas passé!</translation>
    </message>
    <message>
        <source>Level %1 is completed!</source>
        <translation type="vanished">Le niveau %1 est passé!</translation>
    </message>
    <message>
        <source>Errors: %1</source>
        <translation type="vanished">Erreurs: %1</translation>
    </message>
    <message>
        <source>Hints: %1</source>
        <translation type="vanished">Invites: %1</translation>
    </message>
</context>
<context>
    <name>settings</name>
    <message>
        <source>Level: </source>
        <translation type="vanished">Niveau:</translation>
    </message>
    <message>
        <source>Speak text</source>
        <translation type="vanished">Dire le texte</translation>
    </message>
    <message>
        <source>Show text</source>
        <translation type="vanished">Montrer le texte</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="vanished">Jouer</translation>
    </message>
</context>
</TS>
